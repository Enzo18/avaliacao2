package br.ucsal.bes20212.gcm.locadora.builder;

import br.ucsal.bes20212.locadora.dominio.Modelo;
import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	
	private static final String PLACA_DEFAULT = null;

	private static final Integer ANOFABRICACAO_DEFAULT = null;

	private static final Modelo MODELO_DEFAULT = null;

	private static final Double VALOR_DIARIA_DEFAULT = 100.00;
	
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = PLACA_DEFAULT;

	private Integer anoFabricacao = ANOFABRICACAO_DEFAULT;

	private Modelo modelo = MODELO_DEFAULT;

	private Double valorDiaria = VALOR_DIARIA_DEFAULT;
	
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;
	
	private VeiculoBuilder() {
		
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder fabricadoEm(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}
	
	public VeiculoBuilder manutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}
	
	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoBuilder mas() {
		return new VeiculoBuilder().comModelo(modelo).comPlaca(placa).comSituacao(situacao).comValorDiaria(valorDiaria).fabricadoEm(anoFabricacao);
	}
	
	public Veiculo build() {
		
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		
		return veiculo;
	}

}
