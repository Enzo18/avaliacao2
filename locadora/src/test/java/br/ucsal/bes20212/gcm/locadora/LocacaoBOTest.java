package br.ucsal.bes20212.gcm.locadora;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20212.gcm.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.locadora.business.LocacaoBO;
import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.locadora.persistence.VeiculoDAO;

public class LocacaoBOTest {

	private static LocacaoBO locacaoBO;
	private static VeiculoDAO veiculoDAOMock;

	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();
	private static List<String> placas = new ArrayList<String>();
	private LocalDate data = LocalDate.now();

	@BeforeAll
	public static void setupAll() throws VeiculoNaoEncontradoException {

		veiculoDAOMock = Mockito.mock(VeiculoDAO.class);
		locacaoBO = new LocacaoBO(veiculoDAOMock);

		Veiculo veiculo1 = VeiculoBuilder.umVeiculo().fabricadoEm(2019).comPlaca("BFA1J022")
				.disponivel().build();
		Veiculo veiculo2 = VeiculoBuilder.umVeiculo().fabricadoEm(2019).comPlaca("KFN7A332")
				.disponivel().build();
		Veiculo veiculo3 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("PKA5J802")
				.disponivel().build();
		Veiculo veiculo4 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("HFG1G393")
				.disponivel().build();
		Veiculo veiculo5 = VeiculoBuilder.umVeiculo().fabricadoEm(2012).comPlaca("HJN4H047")
				.disponivel().build();

		placas.add(veiculo1.getPlaca());
		placas.add(veiculo2.getPlaca());
		placas.add(veiculo3.getPlaca());
		placas.add(veiculo4.getPlaca());
		placas.add(veiculo5.getPlaca());

		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);

		Mockito.when(veiculoDAOMock.obterPorPlacas(placas)).thenReturn(veiculos);

	}

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	@DisplayName("Teste unitário do valor total: período de 3 dias de locação para 5 veículos")
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		Double valorEsperado = 1410.00;

		Double valorAtual = locacaoBO.calcularValorTotalLocacao(placas, 3, data);
		Assertions.assertEquals(valorEsperado, valorAtual);

	}
	
	

}
